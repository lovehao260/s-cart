<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default users for Development
        DB::table('admin_user')->delete();
        $users = array(
            array('username' => 'Admin', 'name' => 'Super', 'password' => bcrypt('Splus@2019')),
            array('username' => 'HaoLM', 'name' => 'MinhHao', 'password' => bcrypt('hao96969797')),
        );

        DB::table('admin_user')->insert($users);

    }
}
